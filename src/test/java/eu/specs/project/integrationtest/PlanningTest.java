package eu.specs.project.integrationtest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.Context;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.CustomServiceLevel;
import eu.specs.datamodel.agreement.slo.ServiceLevelObjective;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Terms;
import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.datamodel.metrics.*;
import eu.specs.datamodel.sla.sdt.*;
import eu.specs.project.integrationtest.utils.JsonDumper;
import eu.specs.project.negotiation.scm.SupplyChainManager;
import org.glassfish.jersey.filter.LoggingFilter;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PlanningTest {

    private String planningApiAddress;
    private String serviceManagerApiAddress;
    private String implementationApiAddress;
    private WebTarget planningApiTarget;
    private WebTarget implementationApiTarget;

    @Before
    public void setUp() throws Exception {
        Properties properties = new Properties();
        properties.load(this.getClass().getResourceAsStream("/app.properties"));
        this.planningApiAddress = properties.getProperty("planning-api.address");
        this.serviceManagerApiAddress = properties.getProperty("service-manager-api.address");
        this.implementationApiAddress = properties.getProperty("implementation-api.address");

        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .register(new LoggingFilter())
                .build();
        planningApiTarget = client.target(planningApiAddress);
        implementationApiTarget = client.target(implementationApiAddress);
    }

    @Test
    public void test() throws Exception {
        AgreementOffer agreementOffer = createTestAgreementOffer();
        String slatId = "3435ad52-0364-4354-a52c-273c4ce03b66";

        System.out.println("Building supply chains...");
        SupplyChainManager supplyChainManager = new SupplyChainManager(serviceManagerApiAddress, planningApiAddress);
        List<SupplyChain> supplyChains = supplyChainManager.buildSupplyChains(agreementOffer, slatId);

        System.out.println("Supply chains returned by the Supply chain manager:");
        System.out.println(JsonDumper.dump(supplyChains));
        System.out.println();

        SupplyChain selectedSupplyChain = supplyChains.get(0);
        assertEquals(selectedSupplyChain.getSlaId(), slatId);
        assertNotNull(selectedSupplyChain.getScaId());

        // get supply chain activity from the Planning
        System.out.println(String.format("Retrieving supply chain activity %s from the Planning...", selectedSupplyChain.getScaId()));
        SupplyChainActivity supplyChainActivity = planningApiTarget
                .path("/sc-activities/{scaId}")
                .resolveTemplate("scaId", selectedSupplyChain.getScaId())
                .request(MediaType.APPLICATION_JSON)
                .get(SupplyChainActivity.class);
        System.out.println("Supply chain activity:");
        System.out.println(JsonDumper.dump(supplyChainActivity));
        System.out.println();

        // create planning activity at Planning
        System.out.println("Creating planning activity at the Planning...");
        PlanningActivity planningActivityData = new PlanningActivity();
        planningActivityData.setSupplyChain(selectedSupplyChain);
        PlanningActivity planningActivity = planningApiTarget
                .path("/plan-activities")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(planningActivityData), PlanningActivity.class);

        System.out.println("Planning activity created:");
        System.out.println(JsonDumper.dump(planningActivity));
        System.out.println();
        assertEquals(planningActivity.getSlaId(), slatId);
        assertEquals(planningActivity.getSupplyChainId(), selectedSupplyChain.getId());
        assertNotNull(planningActivity.getImplActivityId());

        // get implementation activity from the Implementation
        System.out.println("Retrieving implementation activity from the Implementation...");
        ImplActivity implActivity = implementationApiTarget
                .path("/impl-activities/{iaId}")
                .resolveTemplate("iaId", planningActivity.getImplActivityId())
                .request(MediaType.APPLICATION_JSON)
                .get(ImplActivity.class);

        System.out.println("Implementation activity:");
        System.out.println(JsonDumper.dump(implActivity));
        System.out.println();

        System.out.println("Integration test finished successfully.");
    }

    private AgreementOffer createTestAgreementOffer() {
        AgreementOffer agreementOffer = new AgreementOffer();
        agreementOffer.setName("Test Agreement Offer");

        //Context
        Context c = new Context();
        c.setAgreementInitiator("SPECS-CUSTOMER");
        c.setAgreementResponder("SPECS");
        c.setExpirationTime(new Date());
        c.setTemplateName("default");
        agreementOffer.setContext(c);

        ServiceDescriptionTerm sdt = new ServiceDescriptionTerm();
        sdt.setName("Secure Web Server");
        sdt.setServiceName("SecureWebServer");

        //Service descr
        ServiceDescriptionType sd = new ServiceDescriptionType();
        CapabilityType cap = new CapabilityType();
        cap.setDescription("Capability of surviving to security incidents involving a web server, by implementing proper strategies aimed at preserving business continuity, achieved through redundancy and/or diversity.");
        cap.setId("WEBPOOL");
        cap.setName("Web Resilience");

        CapabilityType.ControlFramework cf = new CapabilityType.ControlFramework();
        cf.setFrameworkName("NIST Control framework 800-53 rev. 4");
        cf.setId("NIST_800_53_r4");

        NISTcontrol nistControl = new NISTcontrol();
        nistControl.setId("WEBPOOL_NIST_CP_2");
        nistControl.setImportanceWeight(WeightType.fromValue("MEDIUM"));
        nistControl.setName("CONTINGENCY PLAN - CP-2");
        nistControl.setControlFamily("SC");
        nistControl.setSecurityControl(BigInteger.valueOf(29));
        nistControl.setControlEnhancement(BigInteger.valueOf(0));
        nistControl.setControlDescription("The organization develops a contingency plan for the information system for achieving continuity of operations for mission/business functions. Contingency planning addresses both information system restoration and implementation of alternative mission/business processes when systems are compromised.");

        cf.getSecurityControl().add(nistControl);
        cap.setControlFramework(cf);
        sd.setCapabilities(new ServiceDescriptionType.Capabilities());
        sd.getCapabilities().getCapability().add(cap);

        AbstractMetricType securityMetricType = new AbstractMetricType();
        securityMetricType.setName("Level of Redundancy - ping");
        securityMetricType.setReferenceId("M1_redundancy");
        AbstractMetricType.AbstractMetricDefinition metricDefinition = new AbstractMetricType.AbstractMetricDefinition();
        UnitType unit = new UnitType();
        unit.setName("number of replicas");
        IntervalType interval = new IntervalType();
        interval.setIntervalItemsType("integer");
        interval.setIntervalItemStart("1");

        unit.setIntervalUnit(interval);
        metricDefinition.setUnit(unit);
        ScaleType scale = new ScaleType();
        scale.setQuantitative(QuantitativeValueType.fromValue("Ratio"));
        metricDefinition.setScale(scale);
        metricDefinition.setExpression("The number of active replicas is checked at each observation instant ti, where the ti are chosen \n" +
                "according to an \"ObservationInterval\" specified in ParameterDefinition and the check strategy is specified in RuleDefinition.");
        MetricRuleType metricRule = new MetricRuleType();
        metricRule.setNote("This rule defines how to check the number of active replicas: the check may be done according to different strategies:{ping,heartbit}");
        metricRule.setRuleDefinitionId("AMR_LoR_CheckStrategy");
        metricRule.setValue("ping");

        MetricType.MetricRules mRules = new MetricType.MetricRules();
        mRules.setMetricRule(metricRule);

        MetricType.MetricParameters mParams = new MetricType.MetricParameters();
        MetricParameterType mp = new MetricParameterType();
        mp.setNote("This parameter refers to the time, expressed in seconds, between two subsequent observations");
        mp.setParameterDefinitionId("AMP_LoR_ObservationInterval");
        mp.setValue("2");
        mParams.setMetricParameter(mp);

        MetricType metric = new MetricType();
        metric.getMetricParameters().add(mParams);
        metric.getMetricRules().add(mRules);
        securityMetricType.setAbstractMetricDefinition(metricDefinition);
        securityMetricType.getMetricParameters().add(mParams);
        securityMetricType.getMetricRules().add(mRules);
        sd.setSecurityMetrics(new ServiceDescriptionType.SecurityMetrics());
        sd.getSecurityMetrics().getSecurityMetric().add(securityMetricType);

        //Service Resources
        ServiceDescriptionType.ServiceResources serviceResources = new ServiceDescriptionType.ServiceResources();
        ServiceDescriptionType.ServiceResources.ResourcesProvider provider = new ServiceDescriptionType.ServiceResources.ResourcesProvider();
        provider.setId("aws-ec2");
        provider.setName("Amazon");
        provider.setZone("us-east-1");
        provider.setMaxAllowedVMs(20);
        ServiceDescriptionType.ServiceResources.ResourcesProvider.VM vm = new ServiceDescriptionType.ServiceResources.ResourcesProvider.VM();
        vm.setAppliance("us-east-1/ami-ff0e0696");
        vm.setHardware("t1.micro");
        vm.setDescr("open suse 13.1 on amazon EC2");
        provider.getVM().add(vm);
        serviceResources.getResourcesProvider().add(provider);
        sd.getServiceResources().add(serviceResources);

        sdt.setServiceDescription(sd);

        //Service properties
        ServiceProperties serviceProperty = new ServiceProperties();
        serviceProperty.setName("//specs:capability[@id='WEBPOOL']");
        serviceProperty.setServiceName("SecureWebServer");
        Variable variable = new Variable();
        variable.setName("specs_webpool_M1");
        variable.setMetric("specs.main.java.negotiation.eu/metrics/M1_redundancy");
        variable.setLocation("//specs:securityControl[@nist:id='WEBPOOL_NIST_CP_2'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_5'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_36']");
        serviceProperty.setVariableSet(new ServiceProperties.VariableSet());
        serviceProperty.getVariableSet().getVariables().add(variable);
        Variable v2 = new Variable();
        v2.setName("specs_webpool_M2");
        v2.setLocation("//specs:securityControl[@nist:id='WEBPOOL_NIST_CP_2'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_5'] | //specs:securityControl[@nist:id='WEBPOOL_NIST_SC_29']");
        v2.setMetric("specs.main.java.negotiation.eu/metrics/M2_diversity");
        serviceProperty.getVariableSet().getVariables().add(v2);

        //Guarantee Term
        GuaranteeTerm guaranteeTerm = new GuaranteeTerm();
        guaranteeTerm.setName("//specs:capability[@id='WEBPOOL']");
        guaranteeTerm.setObligated("ServiceProvider");
        guaranteeTerm.setServiceLevelObjective(new ServiceLevelObjective());
        guaranteeTerm.getServiceLevelObjective().setCustomServiceLevel(new CustomServiceLevel());
        ObjectiveList objectiveList = new ObjectiveList();
        SLOType slo1 = new SLOType();
        slo1.setSLOID("webpool_slo1");
        slo1.setMetricREF("Level of Redundancy");
        slo1.setImportanceWeight(WeightType.MEDIUM);
        SLOexpressionType exp1 = new SLOexpressionType();
        exp1.setOneOpExpression(new SLOexpressionType.OneOpExpression());
        exp1.getOneOpExpression().setOperator(OneOpOperator.EQUAL);
        exp1.getOneOpExpression().setOperand("3");
        slo1.setSLOexpression(exp1);
        objectiveList.getSLO().add(slo1);

        SLOType slo2 = new SLOType();
        slo2.setSLOID("webpool_slo2");
        slo2.setMetricREF("Level of Diversity");
        slo2.setImportanceWeight(WeightType.MEDIUM);
        SLOexpressionType exp2 = new SLOexpressionType();
        exp2.setOneOpExpression(new SLOexpressionType.OneOpExpression());
        exp2.getOneOpExpression().setOperator(OneOpOperator.EQUAL);
        exp2.getOneOpExpression().setOperand("2");
        slo2.setSLOexpression(exp2);
        objectiveList.getSLO().add(slo2);

        guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().setObjectiveList(objectiveList);

        agreementOffer.setTerms(new Terms());
        agreementOffer.getTerms().setAll(new Terms.All());
        agreementOffer.getTerms().getAll().getAll().add(sdt);
        agreementOffer.getTerms().getAll().getAll().add(serviceProperty);
        agreementOffer.getTerms().getAll().getAll().add(guaranteeTerm);

        return agreementOffer;
    }
}
