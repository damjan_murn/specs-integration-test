package eu.specs.project.integrationtest.utils;

import java.io.IOException;
import java.util.Properties;

public class AppConfig {

    private String slaManagerApiAddress;
    private String serviceManagerApiAddress;
    private String sloManagerAddress;
    private String planningApiAddress;
    private String implementationApiAddress;
    private String slaTemplateName;

    public AppConfig() throws IOException {
        Properties properties = new Properties();
        properties.load(this.getClass().getResourceAsStream("/app.properties"));
        this.slaManagerApiAddress = properties.getProperty("sla-manager-api.address");
        this.serviceManagerApiAddress = properties.getProperty("service-manager-api.address");
        this.sloManagerAddress = properties.getProperty("slo-manager-api.address");
        this.planningApiAddress = properties.getProperty("planning-api.address");
        this.implementationApiAddress = properties.getProperty("implementation-api.address");
        this.slaTemplateName = properties.getProperty("slaTemplate.name");
    }

    public String getSlaManagerApiAddress() {
        return slaManagerApiAddress;
    }

    public String getServiceManagerApiAddress() {
        return serviceManagerApiAddress;
    }

    public String getSloManagerAddress() {
        return sloManagerAddress;
    }

    public String getPlanningApiAddress() {
        return planningApiAddress;
    }

    public String getImplementationApiAddress() {
        return implementationApiAddress;
    }

    public String getSlaTemplateName() {
        return slaTemplateName;
    }
}
