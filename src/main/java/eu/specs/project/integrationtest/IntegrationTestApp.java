package eu.specs.project.integrationtest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.integrationtest.shared.CollectionType;
import eu.specs.project.integrationtest.utils.AppConfig;
import eu.specs.project.integrationtest.utils.JsonDumper;
import eu.specs.project.negotiation.scm.SupplyChainManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class IntegrationTestApp {
    private static final Logger logger = LogManager.getLogger(SupplyChainManager.class);
    private static AppConfig appConfig;

    public static void main(String[] args) throws Exception {
        appConfig = new AppConfig();
        try {
            new IntegrationTestApp().start();
        } catch (WebApplicationException e) {
            String resp = e.getResponse().readEntity(String.class);
            System.err.println("REST API request failed: " + e.getMessage() + ": " + resp);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() throws Exception {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                        //.register(new LoggingFilter())
                .build();
        WebTarget sloManagerTarget = client.target(appConfig.getSloManagerAddress());
        WebTarget planningTarget = client.target(appConfig.getPlanningApiAddress());
        WebTarget implementationTarget = client.target(appConfig.getImplementationApiAddress());

        String slaTemplateOfferXml = sloManagerTarget
                .path("/sla-templates/{name}")
                .resolveTemplate("name", appConfig.getSlaTemplateName())
                .request(MediaType.TEXT_XML)
                .get(String.class);

        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer slaTemplateOffer = (AgreementOffer) u.unmarshal(new StringReader(slaTemplateOfferXml));
        System.out.println(String.format("SLA template offer %s:", slaTemplateOffer.getName()));
        System.out.println(slaTemplateOfferXml);

        // get SLA offers
        CollectionType slaOffersCollection = sloManagerTarget
                .path("/sla-templates/{slatOfferId}/slaoffers")
                .resolveTemplate("slatOfferId", slaTemplateOffer.getName())
                .request(MediaType.TEXT_XML)
                .post(Entity.entity(slaTemplateOfferXml, MediaType.TEXT_XML_TYPE), CollectionType.class);

        System.out.println("SLA offers collection:");
        System.out.println(JsonDumper.dump(slaOffersCollection));

        // retrieve SLA offer 1
        String slaOfferUrl1 = slaOffersCollection.getItemList().get(0).getValue();
        slaOfferUrl1 = slaOfferUrl1.replace("sla-templates//", ""); // bug fix
        String slaOfferXml1 = client
                .target(slaOfferUrl1)
                .request(MediaType.TEXT_XML)
                .get(String.class);
        AgreementOffer slaOffer1 = (AgreementOffer) u.unmarshal(new StringReader(slaOfferXml1));
        System.out.println(String.format("SLA offer 1 (ID=%s):", slaOffer1.getName()));
        System.out.println(slaOfferXml1);

        // retrieve supply chains collection
        ResourceCollection supplyChainCollection = planningTarget
                .path("/supply-chains")
                .queryParam("slaId", slaTemplateOffer.getName())
                .request(MediaType.APPLICATION_JSON)
                .get(ResourceCollection.class);

        System.out.println("Supply chains collection:");
        System.out.println(JsonDumper.dump(supplyChainCollection));
        System.out.println();

        // retrieve supply chains
        for (ResourceCollection.Item item : supplyChainCollection.getItemList()) {
            SupplyChain supplyChain = client
                    .target(item.getItem())
                    .request(MediaType.APPLICATION_JSON)
                    .get(SupplyChain.class);

            System.out.println(String.format("Supply chain %s:", supplyChain.getId()));
            System.out.println(JsonDumper.dump(supplyChain));
            System.out.println();
        }

        // create planning activity
        System.out.println("Creating planning activity from the SLA template " + slaTemplateOffer.getName());
        Map<String, String> planningActivityData = new HashMap<String, String>();
        planningActivityData.put("sla_id", slaTemplateOffer.getName());
        PlanningActivity planningActivity = planningTarget
                .path("/plan-activities")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(planningActivityData), PlanningActivity.class);

        System.out.println("Planning activity created:");
        System.out.println(JsonDumper.dump(planningActivity));
        System.out.println();

        // retrieve implementation activity
        System.out.println(String.format("Retrieving implementation activity %s from the Implementation...", planningActivity.getImplActivityId()));
        ImplActivity implActivity = implementationTarget
                .path("/impl-activities/{implActId}")
                .resolveTemplate("implActId", planningActivity.getImplActivityId())
                .request(MediaType.APPLICATION_JSON)
                .get(ImplActivity.class);

        System.out.println("Implementation activity:");
        System.out.println(JsonDumper.dump(implActivity));
        System.out.println();

        // retrieve implementation plan
        System.out.println(String.format("Retrieving implementation plan %s from the Implementation...", planningActivity.getActivePlanId()));
        ImplementationPlan implPlan = implementationTarget
                .path("/impl-plans/{implPlanId}")
                .resolveTemplate("implPlanId", planningActivity.getActivePlanId())
                .request(MediaType.APPLICATION_JSON)
                .get(ImplementationPlan.class);

        System.out.println("Implementation plan:");
        System.out.println(JsonDumper.dump(implPlan));
        System.out.println();
    }
}
