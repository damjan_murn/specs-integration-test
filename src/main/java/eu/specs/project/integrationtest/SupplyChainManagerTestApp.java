package eu.specs.project.integrationtest;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.integrationtest.utils.AppConfig;
import eu.specs.project.negotiation.scm.SupplyChainManager;
import eu.specs.project.negotiation.scm.utils.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.List;

public class SupplyChainManagerTestApp {
    private static final Logger logger = LogManager.getLogger(SupplyChainManager.class);
    private static AppConfig appConfig;

    public static void main(String[] args) throws Exception {
        appConfig = new AppConfig();
        new SupplyChainManagerTestApp().testSupplyChainManager();
    }

    public void testSupplyChainManager() throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                this.getClass().getResourceAsStream("/SLAtemplate_NIST.xml"));

        SupplyChainManager supplyChainManager = new SupplyChainManager(
                appConfig.getServiceManagerApiAddress(),
                appConfig.getPlanningApiAddress());
        List<SupplyChain> supplyChains = supplyChainManager.buildSupplyChains(agreementOffer);
        System.out.println("Supply chains generated:");
        System.out.println(JsonDumper.dump(supplyChains));
    }
}